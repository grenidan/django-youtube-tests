from django.test import TestCase
from . import views
from .ytchannel import YTChannel
from urllib.request import urlopen
# Create your tests here.

class TestYTChannel(TestCase):
    """Tests of YTChannel"""
    def setUp(self):
        self.simpleFile = url = 'https://www.youtube.com/feeds/videos.xml?channel_id=' \
            + 'UC300utwSVAYOoRLEqmsprfg'

        self.expected = [{'link': 'https://www.youtube.com/watch?v=bWiM4s3Anl0', 
                          'title': 'CSS: Ejercicio Django CMS con hoja de estilo CSS', 
                          'id': 'bWiM4s3Anl0'},
                         {'link': 'https://www.youtube.com/watch?v=zzR6dV7pdPY', 
                          'title': 'CSS: Introducción al lenguaje', 
                          'id': 'zzR6dV7pdPY'},
                         {'link': 'https://www.youtube.com/watch?v=mlACAkWqjoA', 
                          'title': 'CSS: Exploración en el depurador del navegador', 
                          'id': 'mlACAkWqjoA'}]

    def test_extraer_videos(self):
        xml = urlopen(self.simpleFile)
        videoscanal = YTChannel(xml)
        videos = videoscanal.videos()
        self.assertEqual(videos[0:3], self.expected)

class TestViews(TestCase):
    def setUp(self):
        xml = urlopen('https://www.youtube.com/feeds/videos.xml?channel_id=' \
            + 'UC300utwSVAYOoRLEqmsprfg')
        videoscanal = YTChannel(xml)

    def test_get_barra(self):
        checks = ["<h1><b>Videos seleccionados</b></h1>",
                  "<h1><b>Videos no seleccionados</b></h1>"]
        response = self.client.get('/')
        content = response.content.decode(encoding='UTF-8')
        for check in checks:
            self.assertInHTML(check, content)
