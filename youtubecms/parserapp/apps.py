from django.apps import AppConfig
import urllib.request
from .ytchannel import YTChannel

class ParserappConfig(AppConfig):
    name = 'parserapp'

    def ready(self):
        from .models import Video
        url = 'https://www.youtube.com/feeds/videos.xml?channel_id=UC300utwSVAYOoRLEqmsprfg'
        xml = urllib.request.urlopen(url)
        videoscanal = YTChannel(xml)

        for video in videoscanal.videos():
            try:
                Video.objects.get(nombre=video['title'])
            except Video.DoesNotExist:
                Video(nombre=video['title'], link=video['link'], id=video['id']).save()
